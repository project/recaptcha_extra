## Recaptcha Extra
This module fixes the issue with recaptcha not loading on modal popup.

## REQUIREMENTS
This module requires the following modules:

 __[Recaptcha](https://drupal.org/project/recaptcha)__ uses the Google reCAPTCHA web service to improve the CAPTCHA system. It is tough on bots and easy on humans.

## INSTALLATION
Install as you would normally install any Drupal module.
